﻿using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MailChimpEXE
{
    class Program
    {
        private const string API_USERNAME = "NEEDLERSMAILCHIMPUSER";
        private const string API_PASSWORD = "^dZ=GU6#EQA?r2CS@DBD9p8gj?Wm5!7R9fe8r8xY+_8EN=upb^#ujQcgwj_ytB_%";//"F^?-F=daKN37=_6qeZdtUfg5ka-y&VSc#H6HRBcNXwXEQ*GyzQk4vYgd$3UU$VB-";
        private const string EMAILLIST = "bustamanteb@brdata.com";//"webdev@brdata.com,mikea@brdata.com,support@brdata.com";//"support@brdata.com,webdev@brdata.com";        
        private const string APIURL = "http://localhost:64664/api/MailChimp/";
        //private const string APIURL = "https://bogopamailchimp.brdata.com:443/api/MailChimp/";
        private const int TIMEOUTMILLISECONDS = 2400000;

        static void Main(string[] args)
        {
            //Email types: nearingthreshold, firsttimeshopperpoints, newcustomers, winback
            List<string> ValidEmailTypes = new List<string>() { "birthday", "firstvisit", "newcustomers", "winback", "returningcustomers" };

            string emailType = "";

            //A number parameter is used here to add or substract the number of days the user wishes to be the date
            if (args.Length > 0)
                emailType = args[0];

            if (ValidEmailTypes.Contains(emailType))
            {
                string emailsJsonResponse = "";
                string newCampaignJson = "";
                bool sendEmailResult = false;
                bool stepSuccess = false;

                //if (emailType.Equals("currentcustomers"))
                //{
                //    try
                //    {
                //        /*****************************************STEP ONE*****************************************/
                //        //Gets all the emails that have changed in the last 24 hours then updates the Cloud Customers table optout value
                //        GetCurrentCustomersFromMailChimpList(emailType);

                //        /*****************************************STEP TWO*****************************************/
                //        //Gets all the emails and send to the Current Customers List
                //        emailsJsonResponse = SendCurrentCustomersToMailChimpList(emailType);

                //        //Test response to ensure its whats expected. Sample: [{"id":"1d672feb99ad19b5ac535d8c13a3ebf5","email_address":"bustamanteb@brdata.com"}]
                //        try
                //        {
                //            JArray emailsJsonResponseArr = JArray.Parse(emailsJsonResponse);
                //            if (emailsJsonResponseArr.Count > 0)
                //            {
                //                JObject obj = JObject.Parse(emailsJsonResponseArr[0].ToString());
                //                if (obj.ContainsKey("id") && obj.ContainsKey("email_address"))
                //                    stepSuccess = true;
                //            }
                //        }
                //        catch
                //        {
                //            var err = "<strong>An error occured while updating the Mailchimp List for the email type: " + emailType + "</strong>";
                //            err += "<br /><br /> " + "Error Message: " + emailsJsonResponse;

                //            SendNotificationEmail(EMAILLIST, "Bogopa Mailchimp Email Error", "noreply@brdata.com", err);
                //        }

                //        if (stepSuccess)
                //        {
                //            /*****************************************STEP THREE*****************************************/
                //            //Updates the Date Submitted value to when the email was sent
                //            UpdateCurrentCustomersAsSubmitted(emailType, emailsJsonResponse);
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        var err = "<strong>The following error occured when processing the email type: " + emailType + "</strong>";
                //        err += "<br /><br /> " + "Error Message: " + ex.Message;

                //        SendNotificationEmail(EMAILLIST, "Bogopa Mailchimp Email Error", "noreply@brdata.com", err);
                //    }
                //}
                //else
                //{
                    try
                    {
                        /*****************************************STEP ZERO ;)*****************************************/
                        //UPDATE 7/10/2019 - Gets all the currently subscribed members in Mail Chimp list and unsubscribes them
                        emailsJsonResponse = GetEmailsFromMailChimpList(emailType);
                        
                        //Invokes the Unsubcribe MailChimp Emails web service 
                        UnsubcribeEmailsFromMailChimpList(emailType, emailsJsonResponse);

                        /*****************************************STEP ONE*****************************************/
                        //Invokes the Add Emails web service that returns JSON list of emails and associated email IDs
                        emailsJsonResponse = AddEmailsToMailChimpList(emailType);

                        //Test response to ensure its whats expected. Sample: [{"id":"1d672feb99ad19b5ac535d8c13a3ebf5","email_address":"bustamanteb@brdata.com"}]
                        try
                        {
                            JArray emailsJsonResponseArr = JArray.Parse(emailsJsonResponse);
                            if (emailsJsonResponseArr.Count > 0)
                            {
                                JObject obj = JObject.Parse(emailsJsonResponseArr[0].ToString());
                                if (obj.ContainsKey("id") && obj.ContainsKey("email_address"))
                                    stepSuccess = true;
                            }
                        }
                        catch
                        {
                            var err = "<strong>An error occured while updating the Mailchimp List for the email type: " + emailType + "</strong>";
                            err += "<br /><br /> " + "Error Message: " + emailsJsonResponse;

                            SendNotificationEmail(EMAILLIST, "Fresh Encounter Mailchimp Email Error", "noreply@brdata.com", err);
                        }

                        /*****************************************STEP TWO*****************************************/
                        //Create the new email Campaign
                        if (stepSuccess)//IF STEP ONE WAS CORRECT
                        {
                            newCampaignJson = CreateCampaign(emailType);
                            stepSuccess = false;

                            //Test response to ensure its whats expected. Sample: { "CampaignID": "jkbji877yhiuh"}
                            try
                            {
                                JObject obj = JObject.Parse(newCampaignJson);
                                if (obj.ContainsKey("CampaignID"))
                                    stepSuccess = true;
                            }
                            catch
                            {
                                var err = "<strong>An error occured while creating a campaign for the email type: " + emailType + "</strong>";
                                err += "<br /><br /> " + "Error Message: " + newCampaignJson;

                                SendNotificationEmail(EMAILLIST, "Fresh Encounter Email Error", "noreply@brdata.com", err);
                            }

                            /*****************************************STEP THREE*****************************************/
                            if (stepSuccess)//IF STEP TWO WAS CORRECT
                            {
                                //Invokes the Send MailChimp Emails web service 
                                sendEmailResult = SendMailChimpCampaignEmails(emailType, newCampaignJson);

                                //BASIC RETRY IF FAILED
                                if (!sendEmailResult)
                                    sendEmailResult = SendMailChimpCampaignEmails(emailType, newCampaignJson);
                            }

                            /*****************************************STEP FOUR*****************************************/
                            if (stepSuccess)
                                //Invokes the Unsubcribe MailChimp Emails web service 
                                UnsubcribeEmailsFromMailChimpList(emailType, emailsJsonResponse);

                            if (stepSuccess && sendEmailResult)
                                //Invokes the update Emails web service to ensure they are not submitted again
                                UpdateEmails(emailType, emailsJsonResponse);

                            //Invokes the Send MailChimp Emails web service 
                            //DeleteMailChimpCampaign(emailType, newCampaignJson);
                        }
                    }
                    catch (Exception ex)
                    {
                        var err = "<strong>The following error occured when processing the email type: " + emailType + "</strong>";
                        err += "<br /><br /> " + "Error Message: " + ex.Message;

                        SendNotificationEmail(EMAILLIST, "Fresh Encounter Mailchimp Email Error", "noreply@brdata.com", err);
                    }
                //}
            }
            else
            {
                var err = "<strong>The following incorrect email type was used for the Mailchimp process: " + emailType + "</strong>";

                SendNotificationEmail(EMAILLIST, "Fresh Encounter Mailchimp Email Error", "noreply@brdata.com", err);
            }
        }

        private static string CreateCampaign(string emailType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}Campaign?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS; //5mins

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonBody);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch(WebException ex)
            {
                return ex.Message;
            }
        }

        private static string AddEmailsToMailChimpList(string emailType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}List?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "GET";
                request.ContentType = "application/json";
                //request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS;// 300000; //5mins

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonBody);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (WebException ex)
            {
                return "[]";// @"[{""id"": ""1d672feb99ad19b5ac535d8c13a3ebf5"",""email_address"": ""bustamanteb@brdata.com""}]";//"[]";
            }
        }

        private static string UpdateEmails(string emailType, string jsonBody)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}List?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.Timeout = TIMEOUTMILLISECONDS; //5mins

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(jsonBody);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (Exception ex)
            {
                return "[]";
            }
        }

        private static bool SendMailChimpCampaignEmails(string emailType, string jsonBody)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}Campaign/Send?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Timeout = TIMEOUTMILLISECONDS; //5mins

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(jsonBody);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                JObject res = JObject.Parse(jsonResponse);
                bool sendMailSuccess = false;

                if (res.ContainsKey("success"))
                    sendMailSuccess = (bool)res["success"];

                return sendMailSuccess;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static string DeleteMailChimpCampaign(string emailType, string jsonBody)
        {
            HttpWebRequest request = WebRequest.Create($"{APIURL}Campaign?e={emailType}") as HttpWebRequest;
            request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
            request.Method = "DELETE";
            request.ContentType = "application/json";
            request.Timeout = TIMEOUTMILLISECONDS; //5mins

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(jsonBody);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

            string jsonResponse = reader.ReadToEnd();

            return jsonResponse;
        }

        private static void UnsubcribeEmailsFromMailChimpList(string emailType, string jsonBody)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}List/Unsubscribe?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "PUT";
                request.ContentType = "application/json";
                //request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS; //5mins

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(jsonBody);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                // return "[]";
            }
        }

        private static void DeleteEmailsFromMailChimpList(string emailType, string jsonBody)
        {
            HttpWebRequest request = WebRequest.Create($"{APIURL}List?e={emailType}") as HttpWebRequest;
            request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
            request.Method = "DELETE";
            request.ContentType = "application/json";
            //request.ContentLength = 0;
            request.Timeout = TIMEOUTMILLISECONDS; //5mins

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(jsonBody);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

            string jsonResponse = reader.ReadToEnd();
        }

        private static string GetEmailsFromMailChimpList(string emailType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}ListMembers?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS;// 300000; //5mins

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonBody);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (Exception ex)
            {
                return "[]";
            }
        }

        private static string GetCurrentCustomersFromMailChimpList(string emailType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}CurrentMembers?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS;// 300000; //5mins

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonBody);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (Exception ex)
            {
                return "[]";
            }
        }

        private static string SendCurrentCustomersToMailChimpList(string emailType)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}CurrentMembers?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS;// 300000; //5mins

                //using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                //{
                //    streamWriter.Write(jsonBody);
                //    streamWriter.Flush();
                //    streamWriter.Close();
                //}

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (Exception ex)
            {
                return "[]";
            }
        }

        private static string UpdateCurrentCustomersAsSubmitted(string emailType, string jsonBody)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create($"{APIURL}CurrentMembers?e={emailType}") as HttpWebRequest;
                request.Credentials = new NetworkCredential(API_USERNAME, API_PASSWORD);
                request.Method = "PUT";
                request.ContentType = "application/json";
                //request.ContentLength = 0;
                request.Timeout = TIMEOUTMILLISECONDS;// 300000; //5mins

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(jsonBody);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                HttpWebResponse myWebResponse = (HttpWebResponse)request.GetResponse();

                StreamReader reader = new StreamReader(myWebResponse.GetResponseStream());

                string jsonResponse = reader.ReadToEnd();

                return jsonResponse;
            }
            catch (Exception ex)
            {
                return "[]";
            }
        }

        public static IRestResponse SendNotificationEmail(string email, string subject, string emailSender, string htmlContent)
        {
            try
            {
                RestClient client = new RestClient();
                client.BaseUrl = new Uri("https://api.mailgun.net/v3");
                client.Authenticator =
                        new HttpBasicAuthenticator("api",
                                                   "key-3ttxmvwru3g5cvqrmoiztefxr9yj31n8");
                //client.Authenticator =
                //        new HttpBasicAuthenticator("api",
                //                                   "key-7e024dabbd1bf7dadbfa3c99aacea6c0");
                RestRequest request = new RestRequest();

                //request.AddParameter("domain", "bronlineshop.brdata.com", ParameterType.UrlSegment);
                request.AddParameter("domain", "brdata.com", ParameterType.UrlSegment);
                request.Resource = "{domain}/messages";
                request.AddParameter("from", emailSender);
                request.AddParameter("to", email);

                request.AddParameter("subject", subject);
                request.AddParameter("html", htmlContent);

                //request.AddFile("inline", attachment);
                request.Method = Method.POST;
                return client.Execute(request);
            }
            catch
            {
                return null;
            }
        }
    }
}
